import React, { Component } from "react";
import "./App.css";
import axios from "axios";

class App extends React.Component {
  state = {
    posts: [],
    isLoading: true,
    errors: null
  };

  getPosts() {
    axios
      .get("https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=3")
      .then(response => {
        this.setState({
          posts: response.data,
          isLoading: false
        });
      });
  }

  componentDidMount() {
    this.getPosts();
  }

  render() {
    const { isLoading, posts } = this.state;
    return (
      <React.Fragment>
        <h2>Curr</h2>
        <div>
          {!isLoading ? (
            posts.map(post => {
              const { buy, sale, ccy } = post;
              return (
                <div key={sale}>
                  <p>{ccy}</p>
                  <p>{buy}</p>
                  <hr />
                </div>
              );
            })
          ) : (
            <p>Loading...</p>
          )}
          <p>{posts.length}</p>
        </div>
      </React.Fragment>
    );
  }
}

export default App;
